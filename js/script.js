"use strict";

let theme = document.querySelector('.them');
let teamStyles = document.querySelector('.teamStyles');

if (localStorage.getItem('href') === null) {
    // teamStyles.setAttribute('href', "./css/style.css");
    localStorage.setItem('href', './css/style.css');
    console.log(localStorage.getItem('href'))
} else {
    const styles = localStorage.getItem('href');
    document.querySelector('.teamStyles').setAttribute('href', `${styles}`);
}

function changeTheme() {
    if (teamStyles.getAttribute('href') === "./css/style.css") {
        teamStyles.setAttribute('href', "./css/styleDark.css");
        localStorage.setItem('href', './css/styleDark.css');
    } else {
        teamStyles.setAttribute('href', "./css/style.css");
        localStorage.setItem('href', './css/style.css');
    }
}

theme.addEventListener('click', () => changeTheme());
